package com.doer.mraims.office.application.port.in.dto.response;

import com.google.gson.GsonBuilder;
import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AddressResponse {

        private String oid;
        private String branchId;
        private String comBranchId;
        private String nameEn;
        private String nameBn;
        private String email;
        private String contactNo;
        private String addressEn;
        private String addressBn;
        private String districtOid;
        private String branchTypeOid;
        private String status;
        @Override
        public String toString(){
                return new GsonBuilder().setPrettyPrinting().create().toJson(this);
        }


}
