package com.doer.mraims.address.application.port.in;


import com.doer.mraims.address.application.port.in.dto.request.AddressSaveRequest;
import com.doer.mraims.core.util.response.CommonListResponseBody;
import com.doer.mraims.core.util.response.CommonObjectResponseBody;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;

import java.util.Map;

public interface AddressFeatureUseCase {

    CommonObjectResponseBody findAddressByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil;

    CommonObjectResponseBody saveAddress(AddressSaveRequest request, Map<String, String> params) throws ExceptionHandlerUtil;

    CommonObjectResponseBody updateAddress(AddressSaveRequest request, String oid, Map<String, String> params) throws ExceptionHandlerUtil;

}
