package com.doer.mraims.address.application.port.in.dto.request;

import com.google.gson.GsonBuilder;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressSaveRequest {

    private String oid;
    private String addressId;

    @NotNull(message = "addressNameEn is required")
    @NotEmpty(message = "addressNameEn can not be empty")
    private String addressNameEn;

    @NotNull(message = "addressNameBn is required")
    @NotEmpty(message = "addressNameBn can not be empty")
    private String addressNameBn;

    @NotNull(message = "addressCode is required")
    @NotEmpty(message = "addressCode can not be empty")
    private String addressCode;

    @NotNull(message = "areaOid is required")
    @NotEmpty(message = "areaOid can not be empty")
    private String areaOid;

    private String status;

    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;

    @Override
    public String toString(){
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

}
