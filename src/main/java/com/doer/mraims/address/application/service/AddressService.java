package com.doer.mraims.address.application.service;

import com.doer.mraims.address.application.port.in.AddressFeatureUseCase;
import com.doer.mraims.address.application.port.in.dto.request.AddressSaveRequest;
import com.doer.mraims.address.application.port.out.AddressPersistencePort;
import com.doer.mraims.address.domain.Address;
import com.doer.mraims.core.util.IdGenerator;
import com.doer.mraims.core.util.MessageCode;
import com.doer.mraims.core.util.ResponseCode;
import com.doer.mraims.core.util.TableStatus;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.core.util.response.CommonObjectResponseBody;
import com.doer.mraims.core.utility.persistance.port.UtilityPersistencePort;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class AddressService implements AddressFeatureUseCase {

    private final AddressPersistencePort addressPersistencePort;
    private final ModelMapper modelMapper;

    private final IdGenerator idGenerator;
    private final UtilityPersistencePort utilityPersistencePort;


    @Override
    public CommonObjectResponseBody findAddressByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        Address address = addressPersistencePort.findByOid(oid, params);
        if (address == null)
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Address data not found by this oid: " + oid);
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(address).build();
        response.setStatus(true);
        response.setCode(ResponseCode.OK.getValue());
        response.setMessageCode(MessageCode.MSG_2003.getCode());
        response.setMessage(MessageCode.MSG_2003.getMessage());
        return response;
    }

    @Override
    public CommonObjectResponseBody saveAddress(AddressSaveRequest request, Map<String, String> params) throws ExceptionHandlerUtil {
        params.put("schemaName", utilityPersistencePort.getSchemaNameByInstituteOid(params.get("instituteOid")));
        int count = addressPersistencePort.countAddress(params.get("schemaName"));
        request.setStatus(TableStatus.ACTIVE.getValueEn());
        request.setAddressId(idGenerator.generateAddressId(count));

        Address requestedAddress = mapRequestToDomain(request, params);
        Address address = addressPersistencePort.save(requestedAddress, params);
        if (address == null) throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Address data not saved");
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(address).build();
        response.setStatus(true);
        response.setCode(ResponseCode.CREATED.getValue());
        response.setMessageCode(MessageCode.MSG_2005.getCode());
        response.setMessage(MessageCode.MSG_2005.getMessage());
        return response;
    }

    @Override
    public CommonObjectResponseBody updateAddress(AddressSaveRequest request, String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        Address address = null;
        params.put("schemaName", utilityPersistencePort.getSchemaNameByInstituteOid(params.get("instituteOid")));
        Address existAddress = addressPersistencePort.findByOid(oid, params);
        if (existAddress != null) {
            request.setOid(oid);
            address = addressPersistencePort.updateState(mapRequestToDomain(request, params), params);
        }
        if (address == null)
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Address data not updated");
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(address).build();
        response.setStatus(true);
        response.setCode(ResponseCode.CREATED.getValue());
        response.setMessageCode(MessageCode.MSG_2007.getCode());
        response.setMessage(MessageCode.MSG_2007.getMessage());
        return response;
    }

    private Address mapRequestToDomain(AddressSaveRequest addressSaveRequest, Map<String, String> params) throws ExceptionHandlerUtil {
        Address existAddress;
        if (addressSaveRequest.getOid() != null) {
            existAddress = addressPersistencePort.findByOid(addressSaveRequest.getOid(), params);
            addressSaveRequest.setCreatedOn(existAddress.getCreatedOn());
            addressSaveRequest.setCreatedBy(existAddress.getCreatedBy());
            addressSaveRequest.setUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
            addressSaveRequest.setUpdatedBy(addressSaveRequest.getUpdatedBy());
        } else {
            addressSaveRequest.setCreatedBy(addressSaveRequest.getCreatedBy());
            addressSaveRequest.setCreatedOn(addressSaveRequest.getCreatedOn() == null ? Timestamp.valueOf(LocalDateTime.now()) : addressSaveRequest.getCreatedOn());
        }
        Address address = modelMapper.map(addressSaveRequest, Address.class);

        return address;
    }


}
