package com.doer.mraims.address.application.port.out;

import com.doer.mraims.address.domain.Address;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;

import java.util.List;
import java.util.Map;

public interface AddressPersistencePort {

    int countAddress(String schema);

    Address save (Address address, Map<String, String> params) throws ExceptionHandlerUtil;

    Address updateState(Address address, Map<String, String> params) throws ExceptionHandlerUtil;

    Address findByAddressId(String addressId, Map<String, String> params) throws ExceptionHandlerUtil;

    Address findByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil;

}
