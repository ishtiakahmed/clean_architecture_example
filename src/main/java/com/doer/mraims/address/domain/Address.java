package com.doer.mraims.address.domain;

import com.google.gson.GsonBuilder;
import lombok.*;

import java.util.Date;

@Data
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Address {
        private String oid;
        private String addressId;
        private String addressNameEn;
        private String addressNameBn;
        private String addressCode;
        private String status;
        private String areaOid;

        private String createdBy;

        private Date createdOn;

        private String updatedBy;

        private Date updatedOn;
        @Override
        public String toString(){
                return new GsonBuilder().setPrettyPrinting().create().toJson(this);
        }


}
