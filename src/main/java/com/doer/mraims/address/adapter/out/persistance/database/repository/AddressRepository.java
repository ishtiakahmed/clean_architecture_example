package com.doer.mraims.address.adapter.out.persistance.database.repository;


import com.doer.mraims.address.adapter.out.persistance.database.entity.AddressEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AddressRepository {
    int countAddress(String schema);

    int countTotalAddressForPagination(Map<String, String> params);

    List<AddressEntity> findAllAddress(Map<String, String> params);

    AddressEntity findByAddressId(String addressId, Map<String, String> params);

    AddressEntity findByOid(String oid, Map<String, String> params);

    int saveWithSchema(AddressEntity entity, Map<String, String> params);

    int updateWithSchema(AddressEntity entity, Map<String, String> params);

}
