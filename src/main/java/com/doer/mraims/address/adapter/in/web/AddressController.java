package com.doer.mraims.address.adapter.in.web;

import com.doer.mraims.address.application.port.in.AddressFeatureUseCase;
import com.doer.mraims.address.application.port.in.dto.request.AddressSaveRequest;
import com.doer.mraims.core.util.response.CommonListResponseBody;
import com.doer.mraims.core.util.response.CommonObjectResponseBody;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@RestController
@RequestMapping("/v1/api/conf/address")
@RequiredArgsConstructor
@Slf4j
public class AddressController {

    private final AddressFeatureUseCase addressFeatureUseCase;


    @GetMapping(path = "/{oid}")
    public CommonObjectResponseBody getAddressByOid(@PathVariable String oid, @RequestParam Map<String, String> params) throws ExceptionHandlerUtil {
        try{
            CommonObjectResponseBody response = addressFeatureUseCase.findAddressByOid(oid, params);
            return response;
        } catch(ExceptionHandlerUtil ex){
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @PostMapping(path = "/save")
    public CommonObjectResponseBody saveAddress(@Valid @RequestBody AddressSaveRequest addressSaveRequest, @RequestParam Map<String, String> params) {
        try{
            log.info("Request got for save Address: {}", addressSaveRequest);
            CommonObjectResponseBody response = addressFeatureUseCase.saveAddress(addressSaveRequest, params);
            log.info("Save response sent for Address: {}", response);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @PutMapping(path = "/update/{oid}")
    public CommonObjectResponseBody updateAddress(@Valid @RequestBody AddressSaveRequest addressSaveRequest, @PathVariable String oid, @RequestParam Map<String, String> params) {
        try{
            log.info("Request got for Address update: {}", addressSaveRequest);
            CommonObjectResponseBody response = addressFeatureUseCase.updateAddress(addressSaveRequest, oid, params);
            log.info("Update response sent for Address: {}", response);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        }catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
}
