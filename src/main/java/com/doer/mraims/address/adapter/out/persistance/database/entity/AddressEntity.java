package com.doer.mraims.address.adapter.out.persistance.database.entity;


import com.google.gson.GsonBuilder;


import jakarta.persistence.*;
import lombok.*;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="address")
public class AddressEntity {
    @Id
    @GeneratedValue(generator = "UUID")

    private String oid;
    private String addressId;
    private String addressNameEn;
    private String addressNameBn;
    private String addressCode;
    private String status;
    private String areaOid;

    private String createdBy;

    private Date createdOn;

    private String updatedBy;

    private Date updatedOn;

    @Override
    public String toString(){
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

}
