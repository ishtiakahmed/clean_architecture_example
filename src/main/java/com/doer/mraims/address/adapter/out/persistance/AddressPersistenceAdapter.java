package com.doer.mraims.address.adapter.out.persistance;

import com.doer.mraims.address.adapter.out.persistance.database.entity.AddressEntity;
import com.doer.mraims.address.adapter.out.persistance.database.repository.AddressRepository;
import com.doer.mraims.address.application.port.out.AddressPersistencePort;
import com.doer.mraims.address.domain.Address;
import com.doer.mraims.core.util.IdGenerator;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.core.util.repository.UtilRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class AddressPersistenceAdapter implements AddressPersistencePort {

    @Autowired
    IdGenerator idGenerator;
    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;
    private final AddressModelMapper addressModelMapper;
    private final UtilRepository utilRepository;

    @Override
    public Address findByAddressId(String addressId, Map<String, String> params) throws ExceptionHandlerUtil{
        AddressEntity addressEntity = addressRepository.findByAddressId(addressId, params);
        if(addressEntity == null){
            throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        }
        return addressModelMapper.mapToDomainEntity(addressEntity);
    }

    @Override
    public Address findByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        String schemaName = utilRepository.findSchemaNameByInstituteOid(params.get("instituteOid"));
        params.put("schemaName", schemaName);
        AddressEntity entity = addressRepository.findByOid(oid, params);
        if (entity == null) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return modelMapper.map(entity, Address.class);
    }

    @Override
    public int countAddress(String schema) {
        return addressRepository.countAddress(schema);
    }

    @Override
    public Address save(Address address, Map<String, String> params) throws ExceptionHandlerUtil {
        String schemaName = utilRepository.findSchemaNameByInstituteOid(params.get("instituteOid"));
        params.put("schemaName", schemaName);
        Provider<AddressEntity> addressEntityProvider = provider -> new AddressEntity();
        AddressEntity entity = addressModelMapper.mapToJpaEntity(addressEntityProvider, address);
        entity.setOid(idGenerator.generateOid());
        int saveCount = addressRepository.saveWithSchema(entity, params);
        return saveCount > 0 ? modelMapper.map(entity, Address.class) : null;
    }

    @Override
    public Address updateState(Address address, Map<String, String> params) throws ExceptionHandlerUtil {
        Provider<AddressEntity> addressEntityProvider = provider -> addressRepository.findByOid(address.getOid(), params);
        AddressEntity entity = addressModelMapper.mapToJpaEntity(addressEntityProvider, address);
        int updateCount = addressRepository.updateWithSchema(entity, params);
        return updateCount > 0 ? modelMapper.map(entity, Address.class) : null;
    }
}
