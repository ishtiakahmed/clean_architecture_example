package com.doer.mraims.address.adapter.out.persistance;

import com.doer.mraims.address.adapter.out.persistance.database.entity.AddressEntity;
import com.doer.mraims.address.application.port.in.dto.request.AddressSaveRequest;
import com.doer.mraims.address.domain.Address;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class AddressModelMapper {

//    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;

    public AddressModelMapper(
//            ObjectMapper objectMapper,
            ModelMapper modelMapper) {
//        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
    }

    public Address mapToDomainEntity(AddressEntity addressEntity) throws ExceptionHandlerUtil {
        /*modelMapper.typeMap(OfficeEntity.class, Office.class)
                .addMappings(mapper -> mapper.map(OfficeEntity::getBranchId, Office::setBranchId));
        modelMapper.typeMap(OfficeEntity.class, Office.class)
                .addMappings(mapper -> mapper.map(OfficeEntity::getOid, Office::setOid));*/
        Address address = modelMapper.map(addressEntity, Address.class);

//        try {
//            branch.setStatus(objectMapper.readValue(branchEntity.getStatus(), String.class));
//        } catch (JsonProcessingException e) {
//            log.error("Error occurred during converting intermediate status to domain object", e);
//            throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, "Something went wrong");
//        }
        return address;
    }

    public AddressEntity mapToJpaEntity(Provider<AddressEntity> addressEntityProvider, Address address) throws ExceptionHandlerUtil {
        modelMapper.typeMap(AddressEntity.class, AddressEntity.class).setProvider(addressEntityProvider);
        AddressEntity addressEntity = modelMapper.map(address, AddressEntity.class);
        return addressEntity;
    }

    List<Address> mapJpaListToDomainEntityList(List<AddressEntity> addressEntityList) throws ExceptionHandlerUtil {
        List<Address> addressList = new ArrayList<Address>();
        for (AddressEntity addressEntity : addressEntityList) {
            Address address = new Address();
            BeanUtils.copyProperties(addressEntity, address);
//            try {
            address = modelMapper.map(addressEntity, Address.class);
//            } catch (JsonProcessingException e) {
//                log.error("Error occurred during converting intermediate status to domain object", e);
//                throw new ExceptionHandlerUtil(HttpStatus.BAD_REQUEST, "Something went wrong");
//            }
            addressList.add(address);
        }
        return addressList;
    }

    private Address mapRequestToDomain(AddressSaveRequest addressSaveRequest){
        Address address = modelMapper.map(addressSaveRequest, Address.class);
        return address;
    }}
