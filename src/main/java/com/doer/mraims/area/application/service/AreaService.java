package com.doer.mraims.area.application.service;

import com.doer.mraims.core.util.*;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.core.util.repository.UtilRepository;
import com.doer.mraims.core.util.response.CommonListResponseBody;
import com.doer.mraims.core.util.response.CommonObjectResponseBody;
import com.doer.mraims.core.utility.persistance.port.UtilityPersistencePort;
import com.doer.mraims.area.application.port.in.AreaUseCase;
import com.doer.mraims.area.application.port.in.dto.request.AreaRequest;
import com.doer.mraims.area.application.port.out.AreaPersistencePort;
import com.doer.mraims.area.domain.Area;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class AreaService implements AreaUseCase {

    private final UtilityPersistencePort utilityPersistencePort;
    private final AreaPersistencePort areaPersistencePort;
    private final IdGenerator idGenerator;
    private final ModelMapper modelMapper;

    @Override
    public CommonObjectResponseBody saveArea(AreaRequest request, Map<String, String> params) throws ExceptionHandlerUtil {
        params.put("schemaName", utilityPersistencePort.getSchemaNameByInstituteOid(params.get("instituteOid")));
        int count = areaPersistencePort.countArea(params.get("schemaName"));
        request.setStatus(TableStatus.ACTIVE.getValueEn());
        request.setOid(idGenerator.generateOid());
//        request.setAreaId(idGenerator.generateAreaId(count));
        Area requestedArea = modelMapper.map(request, Area.class);
        Area area = areaPersistencePort.save(requestedArea, params);
        if (area == null)
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Area data not saved");
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(area).build();
        response.setStatus(true);
        response.setCode(ResponseCode.CREATED.getValue());
        response.setMessageCode(MessageCode.MSG_2005.getCode());
        response.setMessage(MessageCode.MSG_2005.getMessage());
        return response;
    }

    @Override
    public CommonListResponseBody<Object> findAllArea(Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            params.put("schemaName", utilityPersistencePort.getSchemaNameByInstituteOid(params.get("instituteOid")));
            List<Area> list = areaPersistencePort.findAllArea(params);
            int existingBranchCount = areaPersistencePort.countTotalAreaForPagination(params);
            CommonListResponseBody<Object> response;
            if (list.isEmpty()) {
                response = CommonListResponseBody.builder().data(new ArrayList<>()).build();
                //            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Area data not found");
            } else {
                response = CommonListResponseBody.builder().data(Arrays.asList(list.toArray())).build();
                response.setStatus(true);
                response.setCode(ResponseCode.OK.getValue());
                response.setMessageCode(MessageCode.MSG_2000.getCode());
                response.setMessage(MessageCode.MSG_2000.getMessage());
                response.setCount(existingBranchCount);
            }
            return response;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Area data not found");
        }
    }

    @Override
    public CommonObjectResponseBody findAreaByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        params.put("schemaName", utilityPersistencePort.getSchemaNameByInstituteOid(params.get("instituteOid")));
        Area area = areaPersistencePort.findByOid(oid, params);
        if (area == null)
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Area data not found by this oid: " + oid);
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(area).build();
        response.setStatus(true);
        response.setCode(ResponseCode.OK.getValue());
        response.setMessageCode(MessageCode.MSG_2003.getCode());
        response.setMessage(MessageCode.MSG_2003.getMessage());
        return response;
    }

    @Override
    public CommonObjectResponseBody updateArea(AreaRequest request, String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        Area area = null;
        params.put("schemaName", utilityPersistencePort.getSchemaNameByInstituteOid(params.get("instituteOid")));
        Area existArea = areaPersistencePort.findByOid(oid, params);
        if (existArea != null) {
            request.setOid(oid);
            area = areaPersistencePort.updateState(mapRequestToDomain(request, params), params);
        }
        if (area == null)
            throw new ExceptionHandlerUtil(HttpStatus.INTERNAL_SERVER_ERROR, "Area data not updated");
        CommonObjectResponseBody response = CommonObjectResponseBody.builder().data(area).build();
        response.setStatus(true);
        response.setCode(ResponseCode.CREATED.getValue());
        response.setMessageCode(MessageCode.MSG_2007.getCode());
        response.setMessage(MessageCode.MSG_2007.getMessage());
        return response;
    }


    private Area mapRequestToDomain(AreaRequest request, Map<String, String> params) throws ExceptionHandlerUtil {
        Area existCategory;
        if (request.getOid() != null) {
            existCategory = areaPersistencePort.findByOid(request.getOid(), params);
            request.setCreatedOn(existCategory.getCreatedOn());
            request.setCreatedBy(existCategory.getCreatedBy());
            request.setUpdatedOn(Timestamp.valueOf(LocalDateTime.now()));
            request.setUpdatedBy(request.getUpdatedBy());
        } else {
            request.setCreatedBy(request.getCreatedBy());
            request.setCreatedOn(request.getCreatedOn() == null ? Timestamp.valueOf(LocalDateTime.now()) : request.getCreatedOn());
        }
        Area area = modelMapper.map(request, Area.class);

        return area;
    }
}
