package com.doer.mraims.area.application.port.in;

import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.core.util.response.CommonListResponseBody;
import com.doer.mraims.core.util.response.CommonObjectResponseBody;
import com.doer.mraims.area.application.port.in.dto.request.AreaRequest;

import java.util.Map;

public interface AreaUseCase {
    CommonObjectResponseBody saveArea(AreaRequest request, Map<String, String> params) throws ExceptionHandlerUtil;

    CommonListResponseBody<Object> findAllArea(Map<String, String> params) throws ExceptionHandlerUtil;

    CommonObjectResponseBody findAreaByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil;

    CommonObjectResponseBody updateArea (AreaRequest request, String oid, Map<String, String> params) throws ExceptionHandlerUtil;
}
