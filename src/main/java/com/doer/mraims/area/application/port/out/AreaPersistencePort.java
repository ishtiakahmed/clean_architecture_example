package com.doer.mraims.area.application.port.out;

import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.area.domain.Area;

import java.util.List;
import java.util.Map;

public interface AreaPersistencePort {
    int countArea(String schema) ;
    Area save(Area area, Map<String, String> params) throws ExceptionHandlerUtil;
    Area findByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil;
    int countTotalAreaForPagination(Map<String, String> params) throws ExceptionHandlerUtil;
    List<Area> findAllArea(Map<String, String> params) throws ExceptionHandlerUtil;
    Area updateState(Area area, Map<String, String> params) throws ExceptionHandlerUtil;
}
