package com.doer.mraims.area.application.port.in.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AreaRequest {

    private String oid;
    private String areaId;

    @NotNull(message = "nameEn is required")
    @NotEmpty(message = "nameEn can not be empty")
    private String nameEn;

    @NotNull(message = "nameBn is required")
    @NotEmpty(message = "nameBn can not be empty")
    private String nameBn;

    private String status;
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
}
