package com.doer.mraims.area.adapter.out.persistance.database.repository;

import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.area.adapter.out.persistance.database.entity.AreaEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AreaRepository {
    int count (String schema);
    int saveArea(AreaEntity entity, Map<String, String> params) throws ExceptionHandlerUtil;
    int updateArea(AreaEntity entity, Map<String, String> params);
    int countTotalAreaForPagination(Map<String, String> params);
    List<AreaEntity> findAllArea(Map<String, String> params);
    AreaEntity findByOid(String oid, Map<String, String> params);
}
