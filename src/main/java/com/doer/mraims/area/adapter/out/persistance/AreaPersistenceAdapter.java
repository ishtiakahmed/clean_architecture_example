package com.doer.mraims.area.adapter.out.persistance;

import com.doer.mraims.core.util.IdGenerator;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.core.util.repository.UtilRepository;
import com.doer.mraims.area.adapter.out.persistance.database.entity.AreaEntity;
import com.doer.mraims.area.adapter.out.persistance.database.repository.AreaRepository;
import com.doer.mraims.area.application.port.out.AreaPersistencePort;
import com.doer.mraims.area.domain.Area;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AreaPersistenceAdapter implements AreaPersistencePort {

    @Autowired
    IdGenerator idGenerator;
    private final AreaRepository areaRepository;
    private final UtilRepository utilRepository;
    private final ModelMapper modelMapper;


    @Override
    public int countArea(String schema) {
        return areaRepository.count(schema);
    }

    @Override
    public Area save(Area area, Map<String, String> params) throws ExceptionHandlerUtil {
        AreaEntity entity = modelMapper.map(area, AreaEntity.class);
        int saveCount = areaRepository.saveArea(entity, params);
        return saveCount > 0 ? modelMapper.map(entity, Area.class): null;
    }

    @Override
    public Area findByOid(String oid, Map<String, String> params) throws ExceptionHandlerUtil {
        AreaEntity entity = areaRepository.findByOid(oid, params);
        if (entity == null) throw new ExceptionHandlerUtil(HttpStatus.NOT_FOUND, "Data not found");
        return modelMapper.map(entity, Area.class);
    }

    @Override
    public int countTotalAreaForPagination(Map<String, String> params) throws ExceptionHandlerUtil {
        return areaRepository.countTotalAreaForPagination(params);
    }

    @Override
    public List<Area> findAllArea(Map<String, String> params) throws ExceptionHandlerUtil {
        List<AreaEntity> entityList = areaRepository.findAllArea(params);
        return entityList.size() > 0 ? entityList.stream().map(entity -> modelMapper.map(entity, Area.class)).collect(Collectors.toList()) : new ArrayList<>();
    }

    @Override
    public Area updateState(Area area, Map<String, String> params) throws ExceptionHandlerUtil {
        AreaEntity entity = modelMapper.map(area, AreaEntity.class);
        int updateCount = areaRepository.updateArea(entity, params);
        return updateCount > 0 ? modelMapper.map(entity, Area.class) : null;
    }
}
