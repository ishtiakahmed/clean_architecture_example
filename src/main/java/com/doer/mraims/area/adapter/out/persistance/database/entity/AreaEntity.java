package com.doer.mraims.area.adapter.out.persistance.database.entity;

import com.doer.mraims.core.util.model.BaseEntity;
import com.google.gson.GsonBuilder;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@Entity
//@Table(name ="area")
public class AreaEntity extends BaseEntity {

    private String areaId;
    private String nameEn;
    private String nameBn;
    private String areaCode;
    private String zoneId;

    private String zoneNameEn;
    private String zoneNameBn;

}
