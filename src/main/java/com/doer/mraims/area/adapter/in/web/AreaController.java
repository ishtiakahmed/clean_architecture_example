package com.doer.mraims.area.adapter.in.web;

import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.core.util.response.CommonListResponseBody;
import com.doer.mraims.core.util.response.CommonObjectResponseBody;
import com.doer.mraims.area.application.port.in.AreaUseCase;
import com.doer.mraims.area.application.port.in.dto.request.AreaRequest;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

import static com.doer.mraims.core.util.Api.*;

@CrossOrigin(origins = SERVER_BASE)
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(API_BASE + AREA_BASE_PATH)
public class AreaController {

    private final AreaUseCase areaUseCase;

    @PostMapping(path = "/save")
    public CommonObjectResponseBody saveArea (@Valid @RequestBody AreaRequest request, @RequestParam Map<String, String> params) {
        try{
            log.info("Request got for save Area: {}", request);
            CommonObjectResponseBody response = areaUseCase.saveArea(request, params);
            log.info("Save response sent for Area: {}", response);

            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @PutMapping(path = "/update/{oid}")
    public CommonObjectResponseBody updateArea(@Valid @RequestBody AreaRequest request, @PathVariable String oid, @RequestParam Map<String, String> params) {
        System.out.println("Hello");
        try {
            log.info("Request got for Area update: {}", request);
            CommonObjectResponseBody response = areaUseCase.updateArea(request, oid, params);
            log.info("Update response sent for Area: {}", response);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @GetMapping(path = "/{oid}")
    public CommonObjectResponseBody getAreaByOid(@PathVariable String oid, @RequestParam Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            CommonObjectResponseBody response = areaUseCase.findAreaByOid(oid, params);
            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @GetMapping(path = "/list")
    public CommonListResponseBody getAreaList(@RequestParam Map<String, String> params) throws ExceptionHandlerUtil {
        try {
            CommonListResponseBody response = areaUseCase.findAllArea(params);
            log.info("List response sent for Area: {}", response);

            return response;
        } catch (ExceptionHandlerUtil ex) {
            throw new ResponseStatusException(ex.code, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
}
