package com.doer.mraims.area.domain;

import com.doer.mraims.core.util.model.BaseEntity;
import com.google.gson.GsonBuilder;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Area extends BaseEntity {

    private String areaId;
    private String nameEn;
    private String nameBn;

    private String zoneNameEn;
    private String zoneNameBn;
}
