-- templatedb.branch definition

-- Drop table

-- DROP TABLE templatedb.branch;

CREATE TABLE templatedb.branch (
	"oid" varchar(128) NOT NULL,
	branch_id varchar(32) NULL,
	com_branch_id varchar(32) NULL,
	name_en varchar(128) NOT NULL,
	name_bn varchar(128) NULL,
	email varchar(128) NULL,
	contact_no varchar(128) NULL,
	address_en text NULL,
	address_bn text NULL,
	district_oid varchar(128) NULL,
	branch_type_oid varchar(128) NULL,
	status varchar(32) NOT NULL DEFAULT 'Active'::character varying,
	created_by varchar(128) NOT NULL DEFAULT 'System'::character varying,
	created_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by varchar(128) NULL,
	updated_on timestamp NULL,
	CONSTRAINT pk_branch PRIMARY KEY (oid)
);
