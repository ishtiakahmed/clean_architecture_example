INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-001', 'B-0001', 'CBR-1001', 'Barguna Branch', 'বরগুনা শাখা', 'barguna@template.com.bd', NULL, NULL, NULL, 'BD-Barguna', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-002', 'B-0002', 'CBR-1002', 'Barisal Branch', 'বরিশাল শাখা', 'barisal@template.com.bd', NULL, NULL, NULL, 'BD-Barisal', 'MRA-IMS-Branch-Type-Oid-002', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-003', 'B-0003', 'CBR-1003', 'Bhola Branch', 'ভোলা শাখা', 'bhola@template.com.bd', NULL, NULL, NULL, 'BD-Bhola', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-004', 'B-0004', 'CBR-1004', 'Jhalokati Branch', 'ঝালকাঠি শাখা', 'jhalokati@template.com.bd', NULL, NULL, NULL, 'BD-Jhalokati', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-005', 'B-0005', 'CBR-1005', 'Patuakhali Branch', 'পটুয়াখালী শাখা', 'patuakhali@template.com.bd', NULL, NULL, NULL, 'BD-Patuakhali', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-006', 'B-0006', 'CBR-1006', 'Pirojpur Branch', 'পিরোজপুর শাখা', 'pirojpur@template.com.bd', NULL, NULL, NULL, 'BD-Pirojpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-007', 'B-0007', 'CBR-1007', 'Bandarban Branch', 'বান্দরবান শাখা', 'bandarban@template.com.bd', NULL, NULL, NULL, 'BD-Bandarban', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-008', 'B-0008', 'CBR-1008', 'Brahmanbaria Branch', 'ব্রাহ্মণবাড়িয়া শাখা', 'brahmanbaria@template.com.bd', NULL, NULL, NULL, 'BD-Brahmanbaria', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-009', 'B-0009', 'CBR-1009', 'Chandpur Branch', 'চাঁদপুর শাখা', 'chandpur@template.com.bd', NULL, NULL, NULL, 'BD-Chandpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-010', 'B-0010', 'CBR-1010', 'Chittagong Branch', 'চট্টগ্রাম শাখা', 'chittagong@template.com.bd', NULL, NULL, NULL, 'BD-Chittagong', 'MRA-IMS-Branch-Type-Oid-002', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-011', 'B-0011', 'CBR-1011', 'Comilla Branch', 'কুমিল্লা শাখা', 'comilla@template.com.bd', NULL, NULL, NULL, 'BD-Comilla', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-012', 'B-0012', 'CBR-1012', 'Cox''s Bazar Branch', 'কক্সবাজার শাখা', 'coxs bazar@template.com.bd', NULL, NULL, NULL, 'BD-Coxs-Bazar', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-013', 'B-0013', 'CBR-1013', 'Feni Branch', 'ফেনী শাখা', 'feni@template.com.bd', NULL, NULL, NULL, 'BD-Feni', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-014', 'B-0014', 'CBR-1014', 'Khagrachhari Branch', 'খাগড়াছড়ি শাখা', 'khagrachhari@template.com.bd', NULL, NULL, NULL, 'BD-Khagrachhari', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-015', 'B-0015', 'CBR-1015', 'Lakshmipur Branch', 'লক্ষ্মীপুর শাখা', 'lakshmipur@template.com.bd', NULL, NULL, NULL, 'BD-Lakshmipur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-016', 'B-0016', 'CBR-1016', 'Noakhali Branch', 'নোয়াখালী শাখা', 'noakhali@template.com.bd', NULL, NULL, NULL, 'BD-Noakhali', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-017', 'B-0017', 'CBR-1017', 'Rangamati Branch', 'রাঙ্গামাটি শাখা', 'rangamati@template.com.bd', NULL, NULL, NULL, 'BD-Rangamati', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-018', 'B-0018', 'CBR-1018', 'Dhaka Branch', 'ঢাকা শাখা', 'dhaka@template.com.bd', NULL, NULL, NULL, 'BD-Dhaka', 'MRA-IMS-Branch-Type-Oid-001', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-019', 'B-0019', 'CBR-1019', 'Faridpur Branch', 'ফরিদপুর শাখা', 'faridpur@template.com.bd', NULL, NULL, NULL, 'BD-Faridpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-020', 'B-0020', 'CBR-1020', 'Gazipur Branch', 'গাজীপুর শাখা', 'gazipur@template.com.bd', NULL, NULL, NULL, 'BD-Gazipur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-021', 'B-0021', 'CBR-1021', 'Gopalganj Branch', 'গোপালগঞ্জ শাখা', 'gopalganj@template.com.bd', NULL, NULL, NULL, 'BD-Gopalganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-022', 'B-0022', 'CBR-1022', 'Kishoreganj Branch', 'কিশোরগঞ্জ শাখা', 'kishoreganj@template.com.bd', NULL, NULL, NULL, 'BD-Kishoreganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-023', 'B-0023', 'CBR-1023', 'Madaripur Branch', 'মাদারীপুর শাখা', 'madaripur@template.com.bd', NULL, NULL, NULL, 'BD-Madaripur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-024', 'B-0024', 'CBR-1024', 'Manikganj Branch', 'মানিকগঞ্জ শাখা', 'manikganj@template.com.bd', NULL, NULL, NULL, 'BD-Manikganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-025', 'B-0025', 'CBR-1025', 'Munshiganj Branch', 'মুন্সীগঞ্জ শাখা', 'munshiganj@template.com.bd', NULL, NULL, NULL, 'BD-Munshiganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-026', 'B-0026', 'CBR-1026', 'Narayanganj Branch', 'নারায়ণগঞ্জ শাখা', 'narayanganj@template.com.bd', NULL, NULL, NULL, 'BD-Narayanganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-027', 'B-0027', 'CBR-1027', 'Narsingdi Branch', 'নরসিংদী শাখা', 'narsingdi@template.com.bd', NULL, NULL, NULL, 'BD-Narsingdi', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-028', 'B-0028', 'CBR-1028', 'Rajbari Branch', 'রাজবাড়ী শাখা', 'rajbari@template.com.bd', NULL, NULL, NULL, 'BD-Rajbari', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-029', 'B-0029', 'CBR-1029', 'Shariatpur Branch', 'শরীয়তপুর শাখা', 'shariatpur@template.com.bd', NULL, NULL, NULL, 'BD-Shariatpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-030', 'B-0030', 'CBR-1030', 'Tangail Branch', 'টাঙ্গাইল শাখা', 'tangail@template.com.bd', NULL, NULL, NULL, 'BD-Tangail', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-031', 'B-0031', 'CBR-1031', 'Bagerhat Branch', 'বাগেরহাট শাখা', 'bagerhat@template.com.bd', NULL, NULL, NULL, 'BD-Bagerhat', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-032', 'B-0032', 'CBR-1032', 'Chuadanga Branch', 'চুয়াডাঙ্গা শাখা', 'chuadanga@template.com.bd', NULL, NULL, NULL, 'BD-Chuadanga', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-033', 'B-0033', 'CBR-1033', 'Jessore Branch', 'যশোর শাখা', 'jessore@template.com.bd', NULL, NULL, NULL, 'BD-Jessore', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-034', 'B-0034', 'CBR-1034', 'Jhenaidah Branch', 'ঝিনাইদহ শাখা', 'jhenaidah@template.com.bd', NULL, NULL, NULL, 'BD-Jhenaidah', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-035', 'B-0035', 'CBR-1035', 'Khulna Branch', 'খুলনা শাখা', 'khulna@template.com.bd', NULL, NULL, NULL, 'BD-Khulna', 'MRA-IMS-Branch-Type-Oid-002', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-036', 'B-0036', 'CBR-1036', 'Kushtia Branch', 'কুষ্টিয়া শাখা', 'kushtia@template.com.bd', NULL, NULL, NULL, 'BD-Kushtia', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-037', 'B-0037', 'CBR-1037', 'Magura Branch', 'মাগুরা শাখা', 'magura@template.com.bd', NULL, NULL, NULL, 'BD-Magura', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-038', 'B-0038', 'CBR-1038', 'Meherpur Branch', 'মেহেরপুর শাখা', 'meherpur@template.com.bd', NULL, NULL, NULL, 'BD-Meherpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-039', 'B-0039', 'CBR-1039', 'Narail Branch', 'নড়াইল শাখা', 'narail@template.com.bd', NULL, NULL, NULL, 'BD-Narail', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-040', 'B-0040', 'CBR-1040', 'Satkhira Branch', 'সাতক্ষীরা শাখা', 'satkhira@template.com.bd', NULL, NULL, NULL, 'BD-Satkhira', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-041', 'B-0041', 'CBR-1041', 'Jamalpur Branch', 'জামালপুর শাখা', 'jamalpur@template.com.bd', NULL, NULL, NULL, 'BD-Jamalpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-042', 'B-0042', 'CBR-1042', 'Mymensingh Branch', 'ময়মনসিংহ শাখা', 'mymensingh@template.com.bd', NULL, NULL, NULL, 'BD-Mymensingh', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-043', 'B-0043', 'CBR-1043', 'Netrokona Branch', 'নেত্রকোণা শাখা', 'netrokona@template.com.bd', NULL, NULL, NULL, 'BD-Netrokona', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-044', 'B-0044', 'CBR-1044', 'Sherpur Branch', 'শেরপুর শাখা', 'sherpur@template.com.bd', NULL, NULL, NULL, 'BD-Sherpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-045', 'B-0045', 'CBR-1045', 'Bogra Branch', 'বগুড়া শাখা', 'bogra@template.com.bd', NULL, NULL, NULL, 'BD-Bogra', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-046', 'B-0046', 'CBR-1046', 'Joypurhat Branch', 'জয়পুরহাট শাখা', 'joypurhat@template.com.bd', NULL, NULL, NULL, 'BD-Joypurhat', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-047', 'B-0047', 'CBR-1047', 'Naogaon Branch', 'নওগাঁ শাখা', 'naogaon@template.com.bd', NULL, NULL, NULL, 'BD-Naogaon', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-048', 'B-0048', 'CBR-1048', 'Natore Branch', 'নাটোর শাখা', 'natore@template.com.bd', NULL, NULL, NULL, 'BD-Natore', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-049', 'B-0049', 'CBR-1049', 'Chapai Nawabganj Branch', 'চাঁপাইনবাবগঞ্জ শাখা', 'chapai nawabganj@template.com.bd', NULL, NULL, NULL, 'BD-Chapai-Nawabganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-050', 'B-0050', 'CBR-1050', 'Pabna Branch', 'পাবনা শাখা', 'pabna@template.com.bd', NULL, NULL, NULL, 'BD-Pabna', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-051', 'B-0051', 'CBR-1051', 'Rajshahi Branch', 'রাজশাহী শাখা', 'rajshahi@template.com.bd', NULL, NULL, NULL, 'BD-Rajshahi', 'MRA-IMS-Branch-Type-Oid-002', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-052', 'B-0052', 'CBR-1052', 'Sirajganj Branch', 'সিরাজগঞ্জ শাখা', 'sirajganj@template.com.bd', NULL, NULL, NULL, 'BD-Sirajganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-053', 'B-0053', 'CBR-1053', 'Dinajpur Branch', 'দিনাজপুর শাখা', 'dinajpur@template.com.bd', NULL, NULL, NULL, 'BD-Dinajpur', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-054', 'B-0054', 'CBR-1054', 'Gaibandha Branch', 'গাইবান্ধা শাখা', 'gaibandha@template.com.bd', NULL, NULL, NULL, 'BD-Gaibandha', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-055', 'B-0055', 'CBR-1055', 'Kurigram Branch', 'কুড়িগ্রাম শাখা', 'kurigram@template.com.bd', NULL, NULL, NULL, 'BD-Kurigram', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-056', 'B-0056', 'CBR-1056', 'Lalmonirhat Branch', 'লালমনিরহাট শাখা', 'lalmonirhat@template.com.bd', NULL, NULL, NULL, 'BD-Lalmonirhat', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-057', 'B-0057', 'CBR-1057', 'Nilphamari Branch', 'নীলফামারী শাখা', 'nilphamari@template.com.bd', NULL, NULL, NULL, 'BD-Nilphamari', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-058', 'B-0058', 'CBR-1058', 'Panchagarh Branch', 'পঞ্চগড় শাখা', 'panchagarh@template.com.bd', NULL, NULL, NULL, 'BD-Panchagarh', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-059', 'B-0059', 'CBR-1059', 'Rangpur Branch', 'রংপুর শাখা', 'rangpur@template.com.bd', NULL, NULL, NULL, 'BD-Rangpur', 'MRA-IMS-Branch-Type-Oid-002', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-060', 'B-0060', 'CBR-1060', 'Thakurgaon Branch', 'ঠাকুরগাঁও শাখা', 'thakurgaon@template.com.bd', NULL, NULL, NULL, 'BD-Thakurgaon', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-061', 'B-0061', 'CBR-1061', 'Habiganj Branch', 'হবিগঞ্জ শাখা', 'habiganj@template.com.bd', NULL, NULL, NULL, 'BD-Habiganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-062', 'B-0062', 'CBR-1062', 'Moulvibazar Branch', 'মৌলভীবাজার শাখা', 'moulvibazar@template.com.bd', NULL, NULL, NULL, 'BD-Moulvibazar', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-063', 'B-0063', 'CBR-1063', 'Sunamganj Branch', 'সুনামগঞ্জ শাখা', 'sunamganj@template.com.bd', NULL, NULL, NULL, 'BD-Sunamganj', 'MRA-IMS-Branch-Type-Oid-003', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);
INSERT INTO templatedb.branch
("oid", branch_id, com_branch_id, name_en, name_bn, email, contact_no, address_en, address_bn, district_oid, branch_type_oid, status, created_by, created_on, updated_by, updated_on)
VALUES('MRA-IMS-Branch-Oid-064', 'B-0064', 'CBR-1064', 'Sylhet Branch', 'সিলেট শাখা', 'sylhet@template.com.bd', NULL, NULL, NULL, 'BD-Sylhet', 'MRA-IMS-Branch-Type-Oid-002', 'Active', 'System', '2023-03-16 11:45:47.974', NULL, NULL);

