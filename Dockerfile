FROM openjdk:17-jdk

# Create a directory for the application
RUN mkdir /app

# Set the working directory

# Copy the executable JAR file from the Gradle build
COPY . /app

# Expose port 8081
EXPOSE 8081

# Set the command to run the application
ENTRYPOINT ["java", "-jar", "/app/build/libs/mraims-mfi-api-0.0.1-SNAPSHOT.jar"]







